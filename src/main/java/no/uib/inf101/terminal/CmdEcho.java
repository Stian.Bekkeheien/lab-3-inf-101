package no.uib.inf101.terminal;

public class CmdEcho implements Command{

    @Override
    public String run(String[] args) {
        String returnString = "";
        for (String el : args){
            returnString += el;
            returnString += " ";
        }
        return  returnString;
    }

    @Override
    public String getName() {
        // TODO Auto-generated method stub
        return "echo";
    }

}